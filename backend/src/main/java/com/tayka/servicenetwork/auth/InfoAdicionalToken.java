package com.tayka.servicenetwork.auth;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

//@Component
public class InfoAdicionalToken implements TokenEnhancer {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        Map<String,Object> inf = new HashMap<>();
        //Usuario usuario = usuarioService.findByUsername(oAuth2Authentication.getName());

        inf.put("nombre", "tayka");
        inf.put("apellido", "tayka");
        inf.put("email", "tayka@gmail.com");

        ((DefaultOAuth2AccessToken) oAuth2AccessToken).setAdditionalInformation(inf);

        return oAuth2AccessToken;
    }
}
