package com.tayka.servicenetwork.web.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "publications")
public class Publications implements Serializable {
    private static final long serialVersionUID = -2525291984764600853L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    @NotBlank
    private String publishTitle;

    @Column(name = "description")
    private String publishDescription;

    @Column(name = "cost")
    private double publishCost;

    @Column(name = "type")
    private String publishType;

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "user_id")
    @JsonIgnoreProperties("user")
    private Users user;
}
