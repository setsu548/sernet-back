package com.tayka.servicenetwork.web.controllers;

import com.tayka.servicenetwork.services.publications.PublicationsServicesImpl;
import com.tayka.servicenetwork.services.users.UserServicesImpl;
import com.tayka.servicenetwork.web.models.Publications;
import com.tayka.servicenetwork.web.models.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/api/publish")
@RestController
public class PublicationController {

    @Autowired
    PublicationsServicesImpl publicationsServices;

    @Autowired
    UserServicesImpl userServices;

    @PostMapping("/register/{userId}/publication")
    public ResponseEntity<?> insertPublicationIntoUser(@RequestBody Publications publication, @PathVariable("userId") Long userId) {
        Users foundUser = userServices.findById(userId);
        Map<String, Object> mensaje = new HashMap<>();

        if (foundUser == null) {
            mensaje.put("mensaje", "El Id ingresado no existe");
            return new ResponseEntity<>(mensaje, HttpStatus.BAD_REQUEST);
        }

        try{
            publication.setUser(foundUser);

            publicationsServices.save(publication);
        }catch (DataAccessException ex){
            mensaje.put("mensaje", "No se puede insertar al usuario");
            mensaje.put("error", ex.getCause() + " : " + ex.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(mensaje, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        System.out.println("Se publico exitosamente!");
        mensaje.put("", publication);
        return new ResponseEntity<>(mensaje, HttpStatus.OK);
    }
}
