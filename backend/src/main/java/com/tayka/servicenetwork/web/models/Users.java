package com.tayka.servicenetwork.web.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "users")
public class Users implements Serializable {

    private static final long serialVersionUID = 4185756313800909901L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    @Column(name = "name")
//    @NotBlank
    private String userName;

    @Column(name = "lastname")
//    @NotBlank
    private String userLastName;

//    @NotBlank
    @Column(name = "dateofbirth")
    private String dateOfBirth;

    @Column(name = "email")
//    @NotBlank
    private String userEmail;

    @Column(name = "location")
    private String userLocation;

    @Column(name = "cellphone")
    private int userCellPhone;

    @Column(name = "country")
    private String userCountry;

    @Column(name = "state")
    private String userState;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "login_id")
    @JsonIgnoreProperties(value = "login")
    private Login login;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "user")
    @JsonIgnoreProperties("user")
    private List<Publications> publication;
}
