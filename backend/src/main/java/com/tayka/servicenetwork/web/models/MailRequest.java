package com.tayka.servicenetwork.web.models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MailRequest {
    private String name;
    private String to;
    private String from;
    private String subject;

    //Others
    private String location;
}
