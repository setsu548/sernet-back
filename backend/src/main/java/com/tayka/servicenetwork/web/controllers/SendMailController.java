package com.tayka.servicenetwork.web.controllers;

import com.tayka.servicenetwork.services.mails.SendMailService;
import com.tayka.servicenetwork.web.models.MailRequest;
import com.tayka.servicenetwork.web.models.MailResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class SendMailController {

    @Autowired
    private SendMailService service;

    public MailResponse sendEmail(MailRequest request) {
        Map<String, Object> model = new HashMap<>();
        model.put("Name", request.getName());
        model.put("location", request.getLocation());
        return service.sendEmail(request, model);

    }

}
