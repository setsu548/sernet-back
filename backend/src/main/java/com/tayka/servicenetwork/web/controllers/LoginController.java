package com.tayka.servicenetwork.web.controllers;

import com.tayka.servicenetwork.auth.util.JwtUtil;
import com.tayka.servicenetwork.services.LoginDetailsService;
import com.tayka.servicenetwork.web.models.AuthenticationRequest;
import com.tayka.servicenetwork.web.models.AuthenticationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("")
public class LoginController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private LoginDetailsService userDetailsServices;

    @Autowired
    private JwtUtil jwtUtil;

    @GetMapping("/hello")
    public String Hello(){
        return "Hello World";
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
            );
        }catch (BadCredentialsException e){
            throw new Exception("Incorrect email or password" + e);
        }
        final UserDetails userDetails = userDetailsServices.loadUserByUsername(authenticationRequest.getUsername());
        final  String jwt = jwtUtil.generateToken(userDetails);

        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }
}
