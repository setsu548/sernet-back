package com.tayka.servicenetwork.web.controllers;

import com.tayka.servicenetwork.services.users.UserServicesImpl;
import com.tayka.servicenetwork.web.models.MailRequest;
import com.tayka.servicenetwork.web.models.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RequestMapping("/api/users")
@RestController
public class UsersController {

    @Autowired
    public UserServicesImpl userServices;

    @Autowired
    public SendMailController sendMail;

    @GetMapping
    public ResponseEntity<?> getAllUsers(){
        List<Users> usersList = userServices.findAllUsers();

        return new ResponseEntity<>(usersList, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/id/{userId}")
    public ResponseEntity<?> getById(@PathVariable("userId") Long userId ){
        Users user = null;
        Map<String, Object> mensaje = new HashMap<>();

        if (userId == null){
            mensaje.put("mensaje", "El cliente ingresado no existe o no tiene un id de registro");
            return new ResponseEntity<>(mensaje, HttpStatus.BAD_REQUEST);
        }

        try{
            user = userServices.findById(userId);
            if (user == null) {
                mensaje.put("mensaje", "No se encontro al usuario puede que no exista");
                return new ResponseEntity<>(mensaje, HttpStatus.BAD_REQUEST);
            }
        }catch (DataAccessException ex){
            mensaje.put("mensaje", "No se encontro al usuario");
            mensaje.put("error", ex.getMessage().concat(" : ").concat(ex.getCause().getMessage()));
            return new ResponseEntity<>(mensaje, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping("/register")
    public ResponseEntity<?> saveUser(@RequestBody Users user){
        Map<String, Object> mensaje = new HashMap<>();
        Users newUser = null;
        MailRequest mail = null;

        if (user == null) {
            mensaje.put("mensaje","El usuario ingresado esta vacio");
            return new ResponseEntity< Map<String, Object>>(mensaje, HttpStatus.BAD_REQUEST);
        }

        try {
            newUser = userServices.saveUsers(user);
        }catch (DataAccessException ex){
            mensaje.put("mensaje","No se pudo ingresar al usuario");
            mensaje.put("error", ex.getMessage().concat(" : ").concat(ex.getCause().getMessage()));
            return new ResponseEntity<>(mensaje, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        System.out.println("El usuario se creo exito!");
        String name = user.getUserName() + " " + user.getUserLastName();
        String to = user.getUserEmail();
        String subject = "Bienvenido";
        String location = user.getUserState().concat(" - ").concat(user.getUserCountry());

        mail = new MailRequest(name,to,"taykaedsonp@gmail.com",subject, location);
        sendMail.sendEmail(mail);


        mensaje.put("mensaje","El usuario se creo exito!");

        mensaje.put("", newUser);
        return new ResponseEntity<>(mensaje, HttpStatus.CREATED);
    }


}
