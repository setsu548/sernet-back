package com.tayka.servicenetwork.services.users;

import com.tayka.servicenetwork.repositories.UsersRepository;
import com.tayka.servicenetwork.web.models.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServicesImpl implements UserServices {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Users> findAllUsers() {
        List<Users> usersList = usersRepository.findAll();
        return usersList;
    }

    @Override
    public Users saveUsers(Users users) {
        return usersRepository.save(users);
    }

    @Override
    @Transactional(readOnly = true)
    public Users findById(Long id) {
        Users user = usersRepository.findById(id).get();
        return user;
    }
}
