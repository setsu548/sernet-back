package com.tayka.servicenetwork.services;

import com.tayka.servicenetwork.web.models.Login;

public interface LoginServices {
    public Login findLoginByUserName (String username);
}
