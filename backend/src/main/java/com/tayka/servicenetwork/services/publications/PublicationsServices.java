package com.tayka.servicenetwork.services.publications;

import com.tayka.servicenetwork.web.models.Publications;

public interface PublicationsServices {

    public Publications save(Publications publication);
}
