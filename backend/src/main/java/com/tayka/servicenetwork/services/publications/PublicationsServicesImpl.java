package com.tayka.servicenetwork.services.publications;

import com.tayka.servicenetwork.repositories.PublicationsRepository;
import com.tayka.servicenetwork.web.models.Publications;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PublicationsServicesImpl implements PublicationsServices {

    @Autowired
    private PublicationsRepository publicationsRepository;

    @Override
    public Publications save(Publications publication) {
        return publicationsRepository.save(publication);
    }
}
