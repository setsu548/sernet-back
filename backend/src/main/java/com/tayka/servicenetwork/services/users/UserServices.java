package com.tayka.servicenetwork.services.users;

import com.tayka.servicenetwork.web.models.Users;

import java.util.List;

public interface UserServices {

    public List<Users> findAllUsers();

    public Users saveUsers(Users users);

    public Users findById(Long id);
}
