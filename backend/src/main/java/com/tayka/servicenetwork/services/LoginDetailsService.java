package com.tayka.servicenetwork.services;

import com.tayka.servicenetwork.repositories.LoginRepository;
import com.tayka.servicenetwork.web.models.Login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class LoginDetailsService implements LoginServices , UserDetailsService {

    @Autowired
    LoginRepository loginRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        Login login = findLoginByUserName(userName);

        return new User(login.getUsername(),login.getPassword(), new ArrayList<>());
    }

    @Override
    public Login findLoginByUserName(String userName) {
        return loginRepository.findByUsername(userName);
    }
}
