package com.tayka.servicenetwork.repositories;

import com.tayka.servicenetwork.web.models.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users, Long> {

}
