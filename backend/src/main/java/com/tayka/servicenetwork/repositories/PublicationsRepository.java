package com.tayka.servicenetwork.repositories;

import com.tayka.servicenetwork.web.models.Publications;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PublicationsRepository extends JpaRepository<Publications, Long> {
}
