package com.tayka.servicenetwork.repositories;

import com.tayka.servicenetwork.web.models.Login;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginRepository extends JpaRepository<Login, Long> {
    public Login findByUsername(String userName);
}
