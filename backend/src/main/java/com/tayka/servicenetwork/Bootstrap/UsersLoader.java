package com.tayka.servicenetwork.Bootstrap;

import com.tayka.servicenetwork.repositories.UsersRepository;
import com.tayka.servicenetwork.web.models.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class UsersLoader implements CommandLineRunner {

    @Autowired
    private final UsersRepository usersRepository;

    public UsersLoader(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        loaderUsersObject();
    }

    private void loaderUsersObject() {
        if (usersRepository.count() == 0){
            //Users user = new Users("edson","lopez","eds","eds",0000);

            //Users lista = usersRepository.save(user);
            //System.out.println(lista.getUserName());
        }

    }
}
