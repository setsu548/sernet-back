package com.tayka.servicenetwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicenetworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServicenetworkApplication.class, args);
    }

}
