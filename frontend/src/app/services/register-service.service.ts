import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Users } from '../home/Users';



@Injectable({
  providedIn: 'root'
})
export class RegisterServiceService {

  private registerUrl = 'http://localhost:8080/api/users/register';
  public httpHeader =  new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) {
  }

  public save(user: Users): Observable<any> {
    return this.http.post<any>(this.registerUrl, user, {headers: this.httpHeader});
  }
}
