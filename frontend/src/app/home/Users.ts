
import { Login } from './Login';

export class Users {
    userId: number;
    userName: string;
    userLastName: string;
    dateOfBirth: string;
    userEmail: string;
    userLocation: string;
    userCellPhone:number;
    userCountry: string;
    userState: string;
    login: Login;
    publication: [];
}
