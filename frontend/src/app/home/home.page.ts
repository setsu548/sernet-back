import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import {RegisterServiceService} from '../services/register-service.service';
import {Router} from '@angular/router';
import { Users } from './Users';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  private user: Users;
  constructor(
      public alertController: AlertController,
      private registerService: RegisterServiceService,
      private route: Router) {}

  async loginPrompt() {
    const alert = await this.alertController.create({
      header: 'Login',
      inputs: [
        {
          name: 'username',
          type: 'text',
          placeholder: 'User Name or Email'
        },
        {
          name: 'password',
          type: 'password',
          id: 'name2-id',
          placeholder: 'Enter Your Password'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log(data);
          }
        }
      ]
    });

    await alert.present();
  }

  async registerPrompt() {
    const alert = await this.alertController.create({
      header: 'Register',
      inputs: [
        {
          name: 'userName',
          type: 'text',
          placeholder: 'Enter Your Name'
        },
        {
          name: 'lastName',
          type: 'text',
          placeholder: 'Enter Your Last Name'
        },
        {
          name: 'dateOfBirth',
          type: 'date',
          placeholder: 'Date'
        },
        {
          name: 'email',
          type: 'text',
          placeholder: 'Enter Your Email'
        },
        {
          name: 'password',
          type: 'password',
          placeholder: 'Enter Your Password'
        },
        {
          name: 'confirmPassword',
          type: 'password',
          placeholder: 'Confirm Your Password'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            this.config(data);
            //console.log(data);
          }
        }
      ]
    });

    await alert.present();
  }

  config(data: any) {
    const register = {
      userName: data.userName,
      userLastName: data.lastName,
      dateOfBirth: data.dateOfBirth,
      userEmail: data.email,
      userLocation: 'boliva', //data.userLocation,
      userCellPhone: 42698314, // data.userCellPhone,
      userCountry: 'bolivia', // data.userCountry,
      userState: 'cochabamba', //data.userState,
      login: {
        username: data.email,
        password: data.password
      }
    };
    
    this.registerService.save(register).subscribe(result => {
      this.gotoLanding();
      console.log(result);
    });
  }

  gotoLanding() {
    this.route.navigate(['/landing']).then();
  }
}
