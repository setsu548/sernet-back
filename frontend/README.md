#Service Network Front End
##Pre-requisites
To run this project you need to be installed the next software:

* ###NodeJs
  - if you use windows you can download from [here](https://nodejs.org/en/download/)
  - if you use debian linux or derived use this:
  > `sudo apt install nodejs` 
* ###Angular and Ionic
  - To install angular you need this command in cmd or git bash
  > `npm install -g @angular/cli@8.3.26` 
  - To install Ionic use the command:
  > `npm install -g @ionic/cli`

##Installing Project
To intall the project yo need to be in the folder of project and run this command:
> ``npm install``
##Running the Project
If you run the project with ionic default run this:
> ``ionic serve`` <br>

If you run the project with angular run this command:
>`ng serve` -> you can use any parameter

if you want to run by default command run this
>`npm run start`
